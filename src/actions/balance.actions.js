export const REQUEST_BALANCE = 'REQUEST_BALANCE';
export const REQUEST_BALANCE_FAILED = 'REQUEST_BALANCE_FAILED';

export const SET_NAS_BALANCE = 'SET_NAS_BALANCE';
export const SET_GOLD_BALANCE = 'SET_GOLD_BALANCE';

export const requestBalance = () => ({
  type: REQUEST_BALANCE,
})

export const requestBalanceFailed = () => ({
  type: REQUEST_BALANCE_FAILED,
})

export const setNasBalance = (amount) => ({
  type: SET_NAS_BALANCE,
  payload: {
    amount,
  }
})

export const setGoldBalance = (amount) => ({
  type: SET_GOLD_BALANCE,
  payload: {
    amount,
  }
})
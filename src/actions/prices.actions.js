export const SET_PRICES = 'SET_PRICES';

export const setPrices = (data) => ({
  type: SET_PRICES,
  payload: {
    data,
  }
})
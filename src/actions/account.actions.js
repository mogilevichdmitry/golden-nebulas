export const SET_LAST_TXS = 'SET_LAST_TXS';

export const setLastTxs = (txs) => {
  return {
  type: SET_LAST_TXS,
  payload: {
    txs,
  }
}};
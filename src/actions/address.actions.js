export const SET_ADDRESS = 'SET_ADDRESS';
export const REMOVE_ADDRESS = 'REMOVE_ADDRESS';

export const setAddress = (address) => ({
  type: SET_ADDRESS,
  payload: {
    address,
  }
});

export const removeAddress = () => ({
  type: REMOVE_ADDRESS,
})
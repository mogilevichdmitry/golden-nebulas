export const SET_SMARTCONTRACT_INFO = 'SET_SMARTCONTRACT_INFO';

export const setSmartcontractInfo = (info) => ({
  type: SET_SMARTCONTRACT_INFO,
  payload: {
    info
  }
})

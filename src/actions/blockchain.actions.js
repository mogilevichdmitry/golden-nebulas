export const SET_BLOCKCHAIN_INFO = 'SET_BLOCKCHAIN_INFO';

export const setBlockchainInfo = (info) => ({
  type: SET_BLOCKCHAIN_INFO,
  payload: {
    info,
  }
});


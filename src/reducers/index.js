import { combineReducers } from 'redux';
import address from './address.reducer';
import balance from './balance.reducer';
import blockchain from './blockchain.reducer';
import smartcontract from './smartcontract.reducer';
import account from './account.reducer';
import prices from './prices.reducer';

const rootReducer = combineReducers({
  balance,
  blockchain,
  smartcontract,
  prices,
  account,
  address
})

export default rootReducer;
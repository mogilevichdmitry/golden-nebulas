import { SET_BLOCKCHAIN_INFO } from '../actions/blockchain.actions';

const blockchainReducer = (state = {}, action) => {
  switch (action.type) {
    case SET_BLOCKCHAIN_INFO: 
      return {
        ...state,
        ...action.payload.info,
      }
    default:
      return state;
  }
}

export default blockchainReducer;
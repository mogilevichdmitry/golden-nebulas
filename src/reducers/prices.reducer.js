import { SET_PRICES } from '../actions/prices.actions';

const pricesReducer = (state = {}, action) => {
  switch (action.type) {
    case SET_PRICES: 
      return {
        ...state,
        ...action.payload.data,
      }
    default:
      return state;
  }
}

export default pricesReducer;
import { SET_ADDRESS, REMOVE_ADDRESS } from '../actions/address.actions';

const addressReducer = (state = '', action) => {
  switch (action.type) {
    case SET_ADDRESS: 
      return action.payload.address;
    case REMOVE_ADDRESS: 
      return '';
    default:
      return state;
  }
}

export default addressReducer;
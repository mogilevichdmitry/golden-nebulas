import { SET_LAST_TXS } from '../actions/account.actions';

const INITIAL_STATE = {
  lastTxs: {},
};

const accountReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_LAST_TXS:
      return {
        ...state,
        lastTxs: action.payload.txs,
      };
    default: 
      return state;
  }
}

export default accountReducer;
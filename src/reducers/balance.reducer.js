import {
  REQUEST_BALANCE, REQUEST_BALANCE_FAILED, SET_NAS_BALANCE, SET_GOLD_BALANCE
} from '../actions/balance.actions';

const INITIAL_STATE = {
  isLoading: false,
  nas: null,
  gold: null,
}

const balanceReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REQUEST_BALANCE: 
      return {
        ...state,
        isLoading: true,
      }
    case REQUEST_BALANCE_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case SET_NAS_BALANCE: 
      return {
        ...state,
        nas: action.payload.amount,
        isLoading: false,
      }
    case SET_GOLD_BALANCE: 
      return {
        ...state,
        gold: action.payload.amount,
        isLoading: false,
      }
    default:
      return state;
  }
}

export default balanceReducer;
import { SET_SMARTCONTRACT_INFO } from '../actions/smartcontract.actions';

const smartcontractReducer = (state = {}, action) => {
  switch (action.type) {
    case SET_SMARTCONTRACT_INFO: 
      return {
        ...state,
        ...action.payload.info,
      }
    default:
      return state;
  }
}

export default smartcontractReducer;
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { requestBalance, requestBalanceFailed, setNasBalance, setGoldBalance } from '../../actions/balance.actions';
import Balance from '../../components/balance/balance';

const mapStateToProps = (state) => ({
  balance: state.balance,
  address: state.address,
  smartcontract: state.smartcontract,
  prices: state.prices,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    requestBalance,
    requestBalanceFailed,
    setNasBalance,
    setGoldBalance,
  }, dispatch),
});

export const BalanceContainer = connect(mapStateToProps, mapDispatchToProps)(Balance);
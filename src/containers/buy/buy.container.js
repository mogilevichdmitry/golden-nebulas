import { connect } from 'react-redux';
import Buy from '../../components/buy/buy';
import { bindActionCreators } from 'redux';
import { setLastTxs } from '../../actions/account.actions';

const mapStateToProps = (state) => ({
  buyPrice: state.prices.buyPriceNAS,
  balance: state.balance,
  address: state.address,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setLastTxs,
  }, dispatch),
});

export const BuyContainer = connect(mapStateToProps, mapDispatchToProps)(Buy);
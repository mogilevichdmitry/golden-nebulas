import { connect } from 'react-redux';
import Sell from '../../components/sell/sell';
import { bindActionCreators } from 'redux';
import { setLastTxs } from '../../actions/account.actions';

const mapStateToProps = (state) => ({
  sellPrice: state.prices.sellPriceNAS,
  balance: state.balance,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setLastTxs,
  }, dispatch),
});

export const SellContainer = connect(mapStateToProps, mapDispatchToProps)(Sell);
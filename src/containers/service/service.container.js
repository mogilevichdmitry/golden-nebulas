import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { removeAddress } from '../../actions/address.actions';
import Service from '../../components/service/service';

const mapStateToProps = (state) => ({
  balance: state.balance,
  smartcontract: state.smartcontract,
  address: state.address,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    removeAddress
  }, dispatch),
});

export const ServiceContainer = connect(mapStateToProps, mapDispatchToProps)(Service);
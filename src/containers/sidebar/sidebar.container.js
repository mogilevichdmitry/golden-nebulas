import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import { setSmartcontractInfo } from '../../actions/smartcontract.actions';
import Sidebar from '../../components/sidebar/sidebar';

const mapStateToProps = (state) => ({
  smartcontract: state.smartcontract,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setSmartcontractInfo
  }, dispatch)
});

export const SidebarContainer = connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(Sidebar);
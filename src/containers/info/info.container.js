import { connect } from 'react-redux';
import Info from '../../components/info/info';

const mapStateToProps = (state) => ({
  smartcontract: state.smartcontract,
  address: state.address,
  account: state.account,
})

export const InfoContainer = connect(mapStateToProps)(Info);
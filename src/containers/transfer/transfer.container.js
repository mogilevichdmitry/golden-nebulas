import { connect } from 'react-redux';
import Transfer from '../../components/transfer/transfer';
import { bindActionCreators } from 'redux';
import { setLastTxs } from '../../actions/account.actions';

const mapStateToProps = (state) => ({
  balance: state.balance,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setLastTxs,
  }, dispatch),
});

export const TransferContainer = connect(mapStateToProps, mapDispatchToProps)(Transfer);
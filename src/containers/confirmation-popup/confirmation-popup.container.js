import { connect } from 'react-redux';
import ConfirmationPopup from '../../components/confirmation-popup/confirmation-popup';

const mapStateToProps = state => ({
  keystore: state.wallet.keystore,
})

export const ConfirmationPopupContainer = connect(mapStateToProps)(ConfirmationPopup);
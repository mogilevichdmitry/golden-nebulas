import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setAddress } from '../../actions/address.actions';
import Introduction from '../../components/introduction/introduction';

const mapStateToProps = state => ({
  address: state.address,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setAddress,
  }, dispatch),
});

export const IntroductionContainer = connect(mapStateToProps, mapDispatchToProps)(Introduction);
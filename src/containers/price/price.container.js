import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Price from '../../components/price/price';
import { setPrices } from '../../actions/prices.actions';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setPrices,
  }, dispatch)
});

export const PriceContainer = connect(null, mapDispatchToProps)(Price);
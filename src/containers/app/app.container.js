import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setBlockchainInfo } from '../../actions/blockchain.actions';
import { setSmartcontractInfo, } from '../../actions/smartcontract.actions';
import { setPrices } from '../../actions/prices.actions';
import { setAddress } from '../../actions/address.actions';
import { setLastTxs } from '../../actions/account.actions';
import { withRouter } from 'react-router';
import App from '../../components/app/app';

const mapStateToProps = state => ({
  address: state.address,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setLastTxs,
    setBlockchainInfo,
    setSmartcontractInfo,
    setPrices,
    setAddress,
  }, dispatch),
});

export const AppContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
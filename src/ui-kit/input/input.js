import React, { Component } from 'react';
import cn from 'classnames';
import css from './input.css';

class Input extends Component {
  static defaultProps = {
    type: 'text',
  }
  
  state = {
    isFocused: false,
  }

  render() {
    const { isFocused } = this.state;
    const { 
      type, placeholder, onChange, value, label, onKeyPress, disabled, error, id, onKeyDown, readOnly, defaultValue,
    } = this.props;

    return (
      <label className={cn(css.container, {
        [css.container_isFocused]: isFocused,
        [css.container_isError]: error,
        [css.container_file]: type === 'file',
      })}>
        <span className={css.label}>{label}</span>
        <input
          id={id}
          className={cn(css.input, {
            [css.input_file]: type === 'file'
          })}
          type={type}
          onChange={onChange}
          value={value}
          disabled={disabled}
          onKeyPress={onKeyPress}
          placeholder={placeholder}
          onFocus={this.onFocus}
          onKeyDown={onKeyDown}
          onBlur={this.onBlur}
          readOnly={readOnly}
          defaultValue={defaultValue}
        />
      </label>
    )
  }

  onFocus = () => {
    if (this.props.onFocus) {
      this.props.onFocus();
    }
    
    this.setState({
      isFocused: true,
    })
  }

  onBlur = () => {
    if (this.props.onBlur) {
      this.props.onBlur();
    }
    this.setState({
      isFocused: false,
    })
  }
}

export default Input;
import React, { Component } from 'react';
import css from './button.css';

class Button extends Component {
  render() {
    return (
      <button
        onClick={this.props.onClick}
        className={css.container}
        disabled={this.props.disabled}
      >
        {this.props.children}
      </button>
    );
  }
}

export default Button;
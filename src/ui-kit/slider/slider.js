import React, { Component } from 'react';
import Slider from 'rc-slider';
import css from './slider.css';

const marks = {
  0: '0',
  25: '25%',
  50: '50%',
  75: '75%',
  100: '100%',
};

class CustomSlider extends Component {
  state = {
    value: this.props.value,
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        value: nextProps.value,
      })
    }
  }

  render() {
    return (
      <div className={css.container}>
        <Slider value={this.state.value} min={0} max={100} marks={marks} onChange={this.onChange} />
        <ul className={css.marks}>
          <li className={css.mark} onClick={() => this.setValue(0)}>0</li>
          <li className={css.mark} onClick={() => this.setValue(25)}>25%</li>
          <li className={css.mark} onClick={() => this.setValue(50)}>50%</li>
          <li className={css.mark} onClick={() => this.setValue(75)}>75%</li>
          <li className={css.mark} onClick={() => this.setValue(100)}>100%</li>
        </ul>
      </div>
    );
  }

  setValue = (value) => {
    this.setState({
      value,
    })

    this.props.onChange(value);
  }

  onChange = (value) => {
    this.props.onChange(value);
  }
}

export default CustomSlider;
import React, { Component } from 'react';
import css from './confirmation-popup.css';
import { Account } from 'nebulas';
import Input from '../../ui-kit/input/input';
import Button from '../../ui-kit/button/button';
import RootCloseWrapper from 'react-overlays/lib/RootCloseWrapper';

class ConfirmationPopup extends Component {
  state = {
    password: '',
    error: '',
  }

  render() {
    return (
      <div className={css.overlay}>
        <RootCloseWrapper onRootClose={this.props.onClose}>
          <div className={css.container}>
            <h2 className={css.title}>Confirmation</h2>
            <Input
              label="Enter Keystore File password:"
              type="password"
              error={this.state.error}
              onChange={this.onChangePassword}
              onKeyDown={this.onKeyDown}
              readOnly={true}
              onFocus={this.onFocus}
              id="password-input"
            />
            <Button onClick={this.onAccessButtonClick}>CONFIRM</Button>
          </div>
        </RootCloseWrapper>
      </div>
    )
  }

  onFocus = () => {
    document.getElementById('password-input').removeAttribute('readonly')
  }

  onKeyDown = (e) => {
    if (e.keyCode == 13){
      this.onAccessButtonClick();
    }
 }

  onChangePassword = (e) => {
    this.setState({
      password: e.target.value, 
    })
  }

  onAccessButtonClick = () => {
    const { password } = this.state;
    const { keystore } = this.props;

    if (keystore.file && password) {
      let account;
      try {
        account = new Account().fromKey(keystore.file, password);
      }
      catch(error) {
        this.setState({
          error: error.message
        })
      }

      if (account) {
        this.setState({
          account,
        })
        this.props.onClose();
        this.props.callback(account);
      }
    }
  }
}

export default ConfirmationPopup;
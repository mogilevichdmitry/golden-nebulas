import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import css from './sidebar.css';
import { PURE } from 'dx-util/lib/react/pure';

@PURE
class Sidebar extends Component {
  render() {
    const { smartcontract } = this.props;
    const { goldPriceSource, goldPriceUSD, nasPriceSource, nasPriceUSD } = smartcontract;

    return (
      <div className={css.container}>
        <Link to='/'>
          <div className={css.logo} />
        </Link>
        <ul className={css.nav}>
          <NavLink className={css.link} activeClassName={css.linkActive} to="/service/buy">
            <span className={css.linkIconWrapper}>
              <svg className={css.linkIcon} xmlns="http://www.w3.org/2000/svg" width="16" height="16">
                <path d="M14.725 6.667H9.333V1.275C9.333.571 8.738 0 8 0c-.737 0-1.333.57-1.333 1.275v5.392H1.275C.571 6.667 0 7.263 0 8c0 .738.57 1.333 1.275 1.333h5.392v5.392C6.667 15.429 7.263 16 8 16c.738 0 1.333-.57 1.333-1.275V9.333h5.392C15.429 9.333 16 8.738 16 8c0-.737-.57-1.333-1.275-1.333z" fillRule="nonzero" fillOpacity=".9"/>
              </svg>
            </span>
            <span className={css.linkText}>Buy</span>
          </NavLink>
          <NavLink className={css.link} activeClassName={css.linkActive} to="/service/sell">
            <span className={css.linkIconWrapper}>
              <svg className={css.linkIcon} xmlns="http://www.w3.org/2000/svg" width="16" height="3">
                <path d="M0 0h16v3H0z" fillRule="nonzero" fillOpacity=".9"/>
              </svg>
            </span>
            <span className={css.linkText}>Sell</span>
          </NavLink>
          <NavLink className={css.link} activeClassName={css.linkActive} to="/service/transfer">
            <span className={css.linkIconWrapper}>
              <svg className={css.linkIcon} xmlns="http://www.w3.org/2000/svg" width="16" height="13">
                <path d="M11.032.427L15.59 5.2c.273.264.409.623.409 1.018 0 .396-.136.75-.41 1.018l-4.558 4.791a1.344 1.344 0 0 1-1.964 0 1.503 1.503 0 0 1 0-2.054l2.191-2.291H1.395C.623 7.682 0 7.032 0 6.227c0-.804.623-1.454 1.39-1.454h9.865L9.064 2.482a1.503 1.503 0 0 1 0-2.055 1.35 1.35 0 0 1 1.968 0z" fillRule="nonzero" fillOpacity=".9"/>
              </svg>
            </span>
            <span className={css.linkText}>Transfer</span>
          </NavLink>
          <NavLink className={css.link} activeClassName={css.linkActive} to="/info">
            <span className={css.linkIconWrapper}>
              <svg className={css.linkIcon} xmlns="http://www.w3.org/2000/svg" width="7" height="20">
                <g fillRule="nonzero">
                  <path d="M4.673 15.626l.581-2.343 1.287-5.188a1.724 1.724 0 0 0-1.258-2.088l-.167-.042a1.735 1.735 0 0 0-.337-.048l.002-.035-.075.034c-.53-.002-1.304.512-1.304.512C1.898 7.476-.16 8.866.146 9.528c.461 1.003 2.715-1.635 2.175.54l-.581 2.343-1.287 5.188c-.23.924.26 1.84 1.183 2.07l.168.041c.112.028.224.044.336.049l-.002.034.076-.034c.53.002 1.303-.512 1.303-.512 1.504-1.048 3.599-2.429 3.294-3.091-.462-1.003-2.678 1.645-2.138-.53z"/>
                  <circle cx="4.436" cy="2.451" r="2.414"/>
                </g>
              </svg>
            </span>
            <span className={css.linkText}>Info</span>
          </NavLink>
        </ul>
        <div className={css.exchange}>
          <div className={css.title}>Exchange rates</div>
          <div className={css.exchangeItem}>
            <div className={css.currency}>Gold</div>
            <div className={css.exchangeItemAside}>
              <div className={css.currencyPrice}>
                {goldPriceUSD}
                <span className={css.currencyPriceSign}>{' '}$</span>
              </div>
              <div className={css.currencySource}>{goldPriceSource}</div>
            </div>
          </div>
          <div className={css.exchangeItem}>
            <div className={css.currency}>NAS</div>
            <div className={css.exchangeItemAside}>
              <div className={css.currencyPrice}>
                {nasPriceUSD}
                <span className={css.currencyPriceSign}>{' '}$</span>
              </div>
              <div className={css.currencySource}>{nasPriceSource}</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Sidebar;

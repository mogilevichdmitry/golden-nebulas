import React, { Component } from 'react';
import axios from 'axios';
import css from './tx-status.css';

class TxStatus extends Component {
  state = {
    txStatus: 2,
    error: '',
  }

  componentDidMount() {
    const { hash } = this.props;
  
    this.updateTxStatus(hash);
    this.updateInterval = setInterval(() => {
      if (this.state.txStatus === 2) {
        this.updateTxStatus(hash)
      }
    }, 3500);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.hash !== nextProps.hash) {
      clearInterval(this.updateInterval);


      this.setState({
        txStatus: 2,
      })

      this.updateTxStatus(nextProps.hash);
      this.updateInterval = setInterval(() => {
        if (this.state.txStatus === 2) {
          this.updateTxStatus(nextProps.hash)
        }
      }, 5000);
  
    }
  }

  componentWillUnmount() {
    clearInterval(this.updateInterval);
  }

  render() {
    const { txStatus } = this.state;
    const { hash, handleClear, action } = this.props;

    return (
      <div className={css.container}>
        {txStatus === 2 && (
          <div className={css.text}>
            Your transaction has been accepted.
          </div>
        )}
        {txStatus !== 2 && (
          <button className={css.clear} onClick={handleClear}>{`← ${action} MORE`}</button>
        )}
        <div className={css.item}>
          <span className={css.key}>Status:</span>
          <span className={css.value}>{this.renderTxStatus()}</span>
        </div>
        <div className={css.item}>
          <span className={css.key}>Tx hash:</span>
          {txStatus === 2 && <span className={css.value}>{hash}</span>}
          {txStatus !==2 && <a target="_blank" href={`https://explorer.nebulas.io/#/tx/${hash}`} className={css.valueLink}>{hash}</a>}
        </div>
      </div>
    );
  }

  updateTxStatus = (hash) => {
    axios.post('https://mainnet.nebulas.io/v1/user/getTransactionReceipt',
      JSON.stringify({
        hash,
      })
    ).then(response => {
      if (response.data && response.data.result) {
        this.setState({
          txStatus: response.data.result.status,
          error: response.data.result.execute_error,
        })
      }
    });
  }

  renderTxStatus = () => {
    const { txStatus, error } = this.state;
    switch(txStatus) {
      case 0:
        return <span className={css.failed}>{`Fail (${error})`}</span>
      case 1:
        return <span className={css.success}>Success</span>
      case 2:
      default:
        return <span className={css.pending}>Pending...</span>
    }
  }
}

export default TxStatus;

import React, { Component } from 'react';
import { withRouter } from 'react-router';
import css from './introduction.css';
import Input from '../../ui-kit/input/input';
import { Account } from 'nebulas';

class Introduction extends Component {
  state = {
    error: '',
  }

  render() {
    const { error } = this.state;

    return (
      <div className={css.container}>
        <h1 className={css.title}>YOU ARE WELCOME !</h1>
        <div className={css.text}>
          Stable token for Nebulas blockchain.
          GOLD Nebulas is NRC-20 token with guaranteed liquidity and stable price.
          Every GOLD token is equal to 1 gram of physical gold.
        </div>
        <div className={css.text}>
          Tokens are guaranteed by smart contract balance,
          you are always able to sell your tokens according to current price of gold
          and NAS native tokens, which are constantly updating. You can always check
          the amount Current provision is always shown on site and always covers all issued tokens,
          no need for audits in any form, your money are safely stored on blockchain and protected by smart contract.
        </div>
        {!this.props.address && (
          <Input
            error={error}
            label="Paste your wallet address: "
            onChange={this.onAddressChange}
            defaultValue={this.props.address}
          />
        )}

        {error && <div className={css.error}>{error}</div>}
      </div>
    )
  }

  onAddressChange = (e) => {
    const { setAddress } = this.props.actions;

    if (Account.isValidAddress(e.target.value)) {
      setAddress(e.target.value);
      this.props.history.push('/service/buy');
      this.setState({
        error: '',
      })
    } else {
      this.setState({
        error: 'Wrong address'
      })
    }
  }
}

export default withRouter(Introduction);

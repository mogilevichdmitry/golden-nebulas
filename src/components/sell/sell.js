import React, { Component } from 'react';
import css from './sell.css';
import { PURE } from 'dx-util/lib/react/pure';
import { PriceContainer as Price } from '../../containers/price/price.container';
import constants from '../../constants';
import TxStatus from '../tx-status/tx-status'; 
import Button from '../../ui-kit/button/button';
import Input from '../../ui-kit/input/input';
import CustomSlider from '../../ui-kit/slider/slider';
import BigNumber from 'bignumber.js';
import { goldFromDecimals } from '../../utils/gold.uitls';
import NebPay from 'nebpay.js';
import { checkExtension } from '../../utils/extension.utils';
import axios from 'axios';

const nebPay = new NebPay();

@PURE
class Sell extends Component {
  state = {
    gold: '',
    nas: '',
    goldError: '',
    nasError: '',
    txHash: '',
    isExtension: true,
    isInvalid: false,
    isTxProcces: false,
    error: '',
    sliderValue: 0,
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.gold && this.props.sellPrice !== nextProps.sellPrice ) {
      const nas = new BigNumber(this.state.gold).multipliedBy(nextProps.sellPrice).toFixed(constants.NAS_DECIMALS);
      this.setState({
        nas
      })
    }
  }

  render() {
    const { gold, nas, goldError, nasError, txHash, isInvalid, error, isTxProcces, isExtension, sliderValue } = this.state;
    const { sellPrice } = this.props;
    
    return (
      <div className={css.container}>
        <h2 className={css.title}>Sell</h2>
        <div className={css.main}>
          <div>
            <div className={css.form}>
              <span className={css.price}>
                <span className={css.priceText}>Current price:</span>
                <Price>{sellPrice}</Price>
              </span>
              <Input
                label="Payment GOLD amount:"
                value={gold}
                disabled={!sellPrice || sellPrice == 0} 
                onChange={this.onChangeGold}
                onBlur={this.onGoldBlur}
                error={goldError}
              />
              <CustomSlider value={sliderValue} onChange={this.onSliderChange} />
              <Input
                label="Receive NAS amount:"
                value={nas}
                disabled={!sellPrice || sellPrice == 0} 
                onChange={this.onChangeNas}
                onBlur={this.onNasBlur}
                error={nasError}
              />
              {!txHash && <Button onClick={this.onButtonClick} disabled={isInvalid}>Sell</Button>}
            </div>

            {error && <div className={css.error}>{error}</div>}
            {txHash && <TxStatus handleClear={this.handleSellAgain} action="SELL" hash={txHash} />}
          </div>
          <div className={css.aside}>
            {!isExtension && <div className={css.extension}>
              Scan QRCode below using 
              <a
                className={css.extensionLink} 
                target="_blank"
                rel="noopener noreferrer"
                href="https://nano.nebulas.io/index_en.html"
              >
                NAS nano
              </a>
              or install
              <a 
                className={css.extensionLink} 
                target="_blank"
                rel="noopener noreferrer"
                href="https://github.com/nebulasio/WebExtensionWallet"
              >
                Web Extension Wallet
              </a>
              first. Thanks!
            </div>}
            {isExtension && isTxProcces && <div className={css.extension}>
              You can scan QRCode below using 
              <a
                className={css.extensionLink} 
                target="_blank"
                rel="noopener noreferrer"
                href="https://nano.nebulas.io/index_en.html"
              >
                NAS nano
              </a>
            </div>}
            {!txHash && <canvas className={css.qr} id="qr"/> }
          </div>
        </div>
      </div>
    )
  }

  onSliderChange = (value) => {
    const { balance, sellPrice } = this.props;
    const newGold = balance.gold.multipliedBy(value).dividedBy(100);
    this.setState({
      gold: newGold.toFixed(constants.GOLD_DECIMALS),
      nas: newGold.multipliedBy(sellPrice).toFixed(constants.NAS_DECIMALS),
      goldError: '',
      nasError: '',
      isInvalid: false,
      error: '',
      sliderValue: value,
    })
  }

  onNasBlur = () => {
    const oldNas = this.state.nas;
    if (oldNas && oldNas.length - 1 > constants.NAS_DECIMALS) {
      this.setState({
        nas: new BigNumber(oldNas).toFixed(constants.NAS_DECIMALS)
      })
    }
  }

  onGoldBlur = () => {
    const oldGold = this.state.gold;
    if (oldGold && oldGold.length - 1 > constants.GOLD_DECIMALS) {
      this.setState({
        gold: new BigNumber(oldGold).toFixed(constants.GOLD_DECIMALS)
      })
    }
  }

  handleSellAgain = () => {
    this.setState({
      txHash: '',
    })
  }

  onChangeNas = (e) => {
    const { balance, sellPrice } = this.props;
    const value = e.target.value.replace(',', '.');
    const regex = /^[0-9]*[.]?[0-9]+$/;
    const goldValue = new BigNumber(value).dividedBy(sellPrice).toFixed(constants.GOLD_DECIMALS);

    if (regex.test(value) && +value !== 0) {
      if (goldValue <= balance.gold.toFixed(constants.GOLD_DECIMALS) && goldValue > 0) {
        this.setState({
          goldError: '',
          nasError: '',
          gold: goldValue,
          isInvalid: false,
          error: ''
        })
      } else {
        this.setState({
          isInvalid: true,
          error: 'Insufficient balance',
          nasError: 'Insufficient balance',
        })
      }
    } else {
      this.setState({
        nasError: 'Invalid value',
        isInvalid: true,
        error: '',
      })
    }

    this.setState({
      nas: value,
    })
  }

  onChangeGold = (e) => {
    const value = e.target.value.replace(',', '.');
    const regex = /^[0-9]*[.]?[0-9]+$/;
    const { sellPrice, balance } = this.props;

    if (regex.test(value) && +value !== 0) {
      if (+value <= balance.gold.toFixed(constants.GOLD_DECIMALS) && value > 0) {
        this.setState({
          goldError: '',
          nasError: '',
          nas: new BigNumber(value).multipliedBy(sellPrice).toFixed(constants.NAS_DECIMALS),
          sliderValue: new BigNumber(value).multipliedBy(100).dividedBy(balance.gold),
          isInvalid: false,
          error: '',
        })
      } else {
        this.setState({
          isInvalid: true,
          error: 'Insufficient balance',
          goldError: 'Insufficient balance',
        })
      }
     
    } else {
      this.setState({
        goldError: 'Invalid value',
        isInvalid: true,
        error: '',
      })
    }

    this.setState({
      gold: value,
    })
  }


  onButtonClick = () => {
    const { nas, gold } = this.state;
    if (!nas || !gold || +gold === 0 || +nas === 0) {
      this.setState({
        goldError: 'Invalid value',
        nasError: 'Invalid value',
      })
    } else {
      this.handleSell();
    }
  }

  handleSell = (account) => {
    const { sellPrice } = this.props;

    const options = {
      qrcode: {
        showQRCode: true,
        container: document.getElementById('qr')
      },
      listener: this.cbResponse,
    }

    this.setState({
      isTxProcces: true,
    })
        
    nebPay.call(
      constants.CONTRACT_ADDRESS,
      0,
      'dispose',
      JSON.stringify([`${sellPrice}`,`${goldFromDecimals(this.state.gold)}`],),
      options
    );

    this.setState({
      isExtension: checkExtension()
    })
  }

  cbResponse = (response) => {
    this.getTransactions();
    this.setState({
      txHash: response.txhash,
      isTxProcces: false,
    })
  }

  getTransactions = () => {
    const { address, actions } = this.props;
    if (address) {
      axios.get(`https://explorer.nebulas.io/main/api/address/${address}`).then(response => {
        const lastTxs = response.data.data.txList.slice(0, 5);
        actions.setLastTxs(lastTxs);
      });
    }
  }
}

export default Sell;
import React, { Component } from 'react';
import css from './service.css';
import { NavLink, Switch, Route } from 'react-router-dom';
import { BuyContainer as Buy } from '../../containers/buy/buy.container';
import { SellContainer as Sell } from '../../containers/sell/sell.container';
import { TransferContainer as Transfer } from '../../containers/transfer/transfer.container';
import NasLogo from '../../resources/nas.png';
import GoldLogo from '../../resources/nas-gold.png';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import { PURE } from 'dx-util/lib/react/pure';
import { updateSmartcontractExtraInfo } from '../../utils/smartcontract.utils';
import Trend from '../trend/trend';

@PURE
class Service extends Component {
  componentDidMount() {
    const { setSmartcontractInfo } = this.props.actions;
    updateSmartcontractExtraInfo(setSmartcontractInfo);

    this.interval = setInterval(() => updateSmartcontractExtraInfo(setSmartcontractInfo), 5000)
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { smartcontract, balance } = this.props;
    const { goldPriceSource, goldPriceUSD, nasPriceSource, nasPriceUSD } = smartcontract;
    return (
      <div className={css.container}>
        <div className={css.sidebar}>
          <div className={css.balance}>
            <div className={css.balanceItem}>
              <img className={css.balanceIcon} src={GoldLogo} alt="GOLD" />
              <span className={css.balanceKey}>GOLD:</span>
              {balance.gold && (
                <span className={css.balanceValue}>
                  {balance.gold.toFixed(9)}
                </span>
              )}
            </div>
            <div className={css.balanceItem}>
              <img className={css.balanceIcon} src={NasLogo} alt="NAS" />
              <span className={css.balanceKey}>NAS:</span>
              {balance.nas && (
                <span className={css.balanceValue}>
                  {balance.nas.toFixed(9)}
                </span>
              )}
            </div>
          </div>
          <Menu>
            <NavLink className={css.link} activeClassName={css.linkActive} to="/service/buy">
              <MenuItem
                style={{ padding: '0 4px'}}
                primaryText="Buy"
              />
            </NavLink>
            <NavLink className={css.link} activeClassName={css.linkActive} to="/service/sell">
              <MenuItem
                style={{ padding: '0 4px'}}
                primaryText="Sell"
              />
            </NavLink>
            <NavLink className={css.link} activeClassName={css.linkActive} to="/service/transfer">
              <MenuItem
                style={{ padding: '0 4px'}}
                primaryText="Transfer"
              />
            </NavLink>
          </Menu>
          <div className={css.info}>
            <h3 className={css.title}>Golden Nebulas Info:</h3>
            <div className={css.infoItem}>
              <div className={css.infoKey}>Total Supply: </div>
              <div className={css.infoValue}>Total Supply: </div>
            </div>
          </div>
          <div className={css.exchange}>
            <h3 className={css.title}>Exchange rates:</h3>
            <div className={css.exchangeItem}>
              <div className={css.exchangeName}>
                Gold
              </div>
              <div className={css.exchangeItemAside}>
                <div className={css.exchangePrice}>{goldPriceUSD ? `${goldPriceUSD} $` : '-'}</div>
                <div className={css.exchangeSource}>{goldPriceSource || '-'}</div>
              </div>
              <Trend value={+goldPriceUSD}/>
            </div>
            <div className={css.exchangeItem}>
              <div className={css.exchangeName}>
                NAS
              </div>
              <div className={css.exchangeItemAside}>
                <div className={css.exchangePrice}>{nasPriceUSD ? `${nasPriceUSD} $` : '-'}</div>
                <div className={css.exchangeSource}>{nasPriceSource || '-'}</div>
              </div>
              <Trend value={+nasPriceUSD}/>
            </div>
          </div>
        </div>
        <div className={css.content}>
          <Switch>
            <Route
              path="/service/buy"
              component={Buy}
            />
            <Route
              path="/service/sell"
              component={Sell}
            />
            <Route
              path="/service/transfer"
              component={Transfer}
            />
          </Switch>
          {smartcontract.hash && <div className={css.smInfo}>
            <span className={css.smInfoText}>Check smart contract:</span>
            <a
              className={css.smInfoLink}
              target="_blank"
              href={`https://explorer.nebulas.io/#/tx/${smartcontract.hash}`}
            >
              {smartcontract.hash}
            </a>
          </div>}
        </div>
      </div>
    )
  }
}

export default Service;
import React, { Component, Fragment } from 'react';
import css from './service.css';
import { Switch, Route } from 'react-router-dom';
import { BalanceContainer as Balance } from '../../containers/balance/balance.container';
import { BuyContainer as Buy } from '../../containers/buy/buy.container';
import { SellContainer as Sell } from '../../containers/sell/sell.container';
import { TransferContainer as Transfer } from '../../containers/transfer/transfer.container';

class Service extends Component {
  render() {
    const { address, actions } = this.props;
    return (
      <div className={css.container}>
        <div className={css.header}>
          <div className={css.walletIcon} />
          {address && <div className={css.wallet}>{address}</div>}
          <button className={css.exit} onClick={actions.removeAddress}>
            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="28">
              <path d="M25 14.583c0 1.693-.33 3.31-.993 4.85-.662 1.541-1.551 2.87-2.67 3.988-1.117 1.118-2.446 2.008-3.987 2.67-1.54.661-3.157.992-4.85.992-1.693 0-3.31-.33-4.85-.993-1.541-.661-2.87-1.551-3.988-2.669-1.118-1.117-2.007-2.447-2.67-3.987A12.162 12.162 0 0 1 0 14.584C0 12.608.437 10.747 1.31 9a12.26 12.26 0 0 1 3.687-4.395A2.058 2.058 0 0 1 6.55 4.2c.57.076 1.023.347 1.36.814.346.456.48.968.398 1.538a2.027 2.027 0 0 1-.806 1.375 8.31 8.31 0 0 0-2.466 2.946 8.186 8.186 0 0 0-.87 3.711c0 1.129.22 2.206.659 3.231a8.397 8.397 0 0 0 1.782 2.661 8.397 8.397 0 0 0 2.661 1.782c1.026.44 2.103.66 3.231.66 1.128 0 2.205-.22 3.23-.66a8.397 8.397 0 0 0 2.662-1.782 8.397 8.397 0 0 0 1.782-2.66c.44-1.026.66-2.103.66-3.232 0-1.313-.29-2.55-.871-3.71a8.31 8.31 0 0 0-2.466-2.947 2.027 2.027 0 0 1-.806-1.375 1.98 1.98 0 0 1 .399-1.538 1.93 1.93 0 0 1 1.367-.814 2.02 2.02 0 0 1 1.546.407 12.26 12.26 0 0 1 3.687 4.395A12.315 12.315 0 0 1 25 14.583zm-10.417-12.5V12.5c0 .564-.206 1.053-.618 1.465-.412.412-.9.618-1.465.618a2.002 2.002 0 0 1-1.465-.618 2.002 2.002 0 0 1-.618-1.465V2.083c0-.564.206-1.052.618-1.465.412-.412.9-.618 1.465-.618.564 0 1.053.206 1.465.618.412.413.618.901.618 1.465z" fillRule="nonzero"/>
            </svg>
          </button>
        </div>
        <Switch>
          <Route path="/service/buy">
            <Fragment>
              <Balance />
              <Buy />
            </Fragment>
          </Route>
          <Route path="/service/sell">
            <Fragment>
              <Balance />
              <Sell />
            </Fragment>
          </Route>
          <Route path="/service/transfer">
            <Fragment>
              <Balance />
              <Transfer />
            </Fragment>
          </Route>
        </Switch>
      </div>
    );
  }
}

export default Service;
import React, { Component } from 'react';
import css from './transfer.css';
import { PURE } from 'dx-util/lib/react/pure';
import constants from '../../constants';
import { goldFromDecimals } from '../../utils/gold.uitls';
import TxStatus from '../tx-status/tx-status';
import BigNumber from 'bignumber.js';
import Button from '../../ui-kit/button/button';
import Input from '../../ui-kit/input/input';
import CustomSlider from '../../ui-kit/slider/slider';
import NebPay from 'nebpay.js';
import { Account } from 'nebulas';
import { checkExtension } from '../../utils/extension.utils';
import axios from 'axios';

const nebPay = new NebPay();

@PURE
class Transfer extends Component {
  state = {
    gold: '',
    goldError: '',
    recipient: '',
    recipientError: '',
    txHash: '',
    isExtension: true,
    error: '',
    isTxProcces: false,
    sliderValue: 0,
  }

  render() {
    const { gold, goldError, recipientError, txHash, error, isTxProcces, isExtension, sliderValue } = this.state;

    return (
      <div className={css.container}>
        <h2 className={css.title}>Transfer</h2>
        <div className={css.main}>
          <div>
            <div className={css.form}>
              <Input
                label="GOLD amount:"
                value={gold}
                onChange={this.onChangeGold}
                onBlur={this.onGoldBlur}
                error={goldError}
              />
              <CustomSlider value={sliderValue} onChange={this.onSliderChange} />
              <Input
                label="Recipient address:"
                onChange={this.onChangeRecipient}
                error={recipientError}
              />
              <Button onClick={this.onButtonClick} disabled={!!(recipientError || goldError)}>Send</Button>
            </div>

            {error && <div className={css.error}>{error}</div>}
            {txHash && <TxStatus handleClear={this.handleTransferMore} action="TRANSFER" hash={txHash} />}
          </div>
          <div className={css.aside}>
            {!isExtension && <div className={css.extension}>
              Scan QRCode below using 
              <a
                className={css.extensionLink} 
                target="_blank"
                rel="noopener noreferrer"
                href="https://nano.nebulas.io/index_en.html"
              >
                NAS nano
              </a>
              or install
              <a 
                className={css.extensionLink} 
                target="_blank"
                rel="noopener noreferrer"
                href="https://github.com/nebulasio/WebExtensionWallet"
              >
                Web Extension Wallet
              </a>
              first. Thanks!
            </div>}
            {isExtension && isTxProcces && <div className={css.extension}>
              You can scan QRCode below using 
              <a
                className={css.extensionLink} 
                target="_blank"
                rel="noopener noreferrer"
                href="https://nano.nebulas.io/index_en.html"
              >
                NAS nano
              </a>
            </div>}
            {!txHash && <canvas className={css.qr} id="qr"/> }
          </div>
        </div>
      </div>
    )
  }

  onSliderChange = (value) => {
    const { balance } = this.props;
    this.setState({
      gold: balance.gold.multipliedBy(value).dividedBy(100).toFixed(constants.GOLD_DECIMALS),
      goldError: '',
      error: '',
      sliderValue: value,
    })
  }

  onGoldBlur = () => {
    const oldGold = this.state.gold;
    if (oldGold && oldGold.length - 1 > constants.GOLD_DECIMALS) {
      this.setState({
        gold: new BigNumber(oldGold).toFixed(constants.GOLD_DECIMALS)
      })
    }
  }
  handleTransferMore = () => {
    this.setState({
      txHash: '',
    })
  }

  onButtonClick = () => {
    const { gold, recipient } = this.state;
    if (!gold) {
      this.setState({
        goldError: 'Empty field'
      })
    }

    if (!recipient) {
      this.setState({
        recipientError: 'Empty field'
      })
    }

    if (gold && recipient) {
      this.handleTransfer();
    }
  }

  onChangeRecipient = (e) => {
    this.setState({
      recipient: e.target.value,
      recipientError: '',
    })
    if(!Account.isValidAddress(e.target.value)) {
      this.setState({
        recipientError: 'Invalid address',
        error: 'Invalid recipient address',
      })
    } else {
      this.setState({
        recipientError: '',
        error: '',
      })
    }
  }

  onChangeGold = (e) => {
    const value = e.target.value.replace(',', '.');
    const regex = /^[0-9]*[.]?[0-9]+$/;
    const { balance } = this.props;

    if (regex.test(value) && +value !== 0) {
      if (+value <= balance.gold.toFixed(constants.GOLD_DECIMALS) && value > 0) {
        this.setState({
          goldError: '',
          error: '',
          sliderValue: new BigNumber(value).multipliedBy(100).dividedBy(balance.gold),
        })
      } else {
        this.setState({
          error: 'Insufficient balance',
          goldError: 'Insufficient balance',
        })
      }
    } else {
      this.setState({
        goldError: 'Invalid value',
      })
    }

    this.setState({
      gold: value,
    })
  }

  handleTransfer = (account) => {
    const { recipient, gold } = this.state;

    const options = {
      qrcode: {
        showQRCode: true,
        container: document.getElementById('qr')
      },
      listener: this.cbResponse,
    }

    this.setState({
      isTxProcces: true,
    })

    nebPay.call(
      constants.CONTRACT_ADDRESS,
      0,
      'transfer',
      JSON.stringify([`${recipient}`, `${goldFromDecimals(gold)}`]),
      options
    );

    this.setState({
      isExtension: checkExtension()
    })
  }

  cbResponse = (response) => {
    this.getTransactions();
    this.setState({
      txHash: response.txhash,
      isTxProcces: false,
    })
  }

  getTransactions = () => {
    const { address, actions } = this.props;
    if (address) {
      axios.get(`https://explorer.nebulas.io/main/api/address/${address}`).then(response => {
        const lastTxs = response.data.data.txList.slice(0, 5);
        actions.setLastTxs(lastTxs);
      });
    }
  }
}

export default Transfer;
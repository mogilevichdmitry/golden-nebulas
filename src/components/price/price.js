import React, { Component } from 'react';
import { updatePrices } from '../../utils/smartcontract.utils';
import { PURE } from 'dx-util/lib/react/pure';
import css from './price.css';
import Trend from '../trend/trend';

@PURE
class Price extends Component {
  componentDidMount() {
    this.interval = setInterval(() => updatePrices(this.props.actions.setPrices), 5000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { children } = this.props;
    return (
      <span className={css.container}>
        {children}
        <Trend value={+children}/>
      </span>
    );
  }
}

export default Price;
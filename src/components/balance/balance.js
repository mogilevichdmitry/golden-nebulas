import React, { Component } from 'react';
import css from './balance.css';
import { NebService } from '../../utils/neb.utils';
import { Unit, Utils } from 'nebulas';
import { updateGoldBalance } from '../../utils/smartcontract.utils';
import { formatBalance } from '../../utils/number.utils';
import Spinner from '../spinner/spinner';

class Balance extends Component {
  componentDidMount() {
    this.updateAccountState(this.props.address);

    this.interval = setInterval(() => this.updateAccountState(this.props.address), 5000)
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  updateAccountState = (address) => {
    const { requestBalance, requestBalanceFailed, setNasBalance, setGoldBalance } = this.props.actions;
    requestBalance();

    NebService.api.getAccountState(address).then(state => {
      setNasBalance(Unit.fromBasic(Utils.toBigNumber(state.balance), 'nas').toNumber());
    })
    .catch(function (err) {
      console.log(err);
      requestBalanceFailed();
    });

    updateGoldBalance(address, setGoldBalance);
  }

  render() {
    const { balance, smartcontract, prices } = this.props;
    const { goldPriceUSD, nasPriceUSD } = smartcontract;

    return (
      <div className={css.container}>
        <div className={css.item}>
          <div className={css.token}>
            <div className={css.gold} />
            <div className={css.name}>GOLD</div>
          </div>
          {balance.gold !== null && <div className={css.sum}>
            <div className={css.amount}>{formatBalance(balance.gold.toFixed(9), 9)}</div>
            <div className={css.amountAlt}>{`≈ ${formatBalance(balance.gold.multipliedBy(prices.sellPriceNAS).toFixed(9), 4)} NAS`}</div>
            <div className={css.amountAlt}>{`≈ ${formatBalance(balance.gold.multipliedBy(goldPriceUSD).toFixed(9), 2)} USD`}</div>
          </div>}
          {!balance.gold && <div className={css.spinner}><Spinner /></div>}
        </div>
        <div className={css.item}>
          <div className={css.token}>
            <div className={css.nas} />
            <div className={css.name}>NAS</div>
          </div>
          {balance.nas !== null && <div className={css.sum}>
            <div className={css.amount}>{formatBalance(balance.nas.toFixed(18), 18)}</div>
            <div className={css.amountAlt}>{`≈ ${formatBalance((balance.nas / prices.buyPriceNAS), 4)} GOLD`}</div>
            <div className={css.amountAlt}>{`≈ ${formatBalance((balance.nas * nasPriceUSD), 2)} USD`}</div>
          </div>}
          {(balance.nas === 'undefined' || balance.nas === null) && <div className={css.spinner}><Spinner /></div>}
        </div>
      </div>
    );
  }
}

export default Balance;

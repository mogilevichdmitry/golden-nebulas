import React, { Component } from 'react';
import css from './info.css';
import cn from 'classnames';
import constants from '../../constants';
import { withRouter } from 'react-router';
import { nasToDecimals } from '../../utils/nas.utils';
import timeago from 'timeago.js';
import Spinner from '../spinner/spinner';

class Info extends Component {
  render() {
    const { address, smartcontract, history, account } = this.props;
    const { contractBalanceNAS, goldSupplyGrams, fairPrice, hash } = smartcontract;
    const provision = (contractBalanceNAS / (goldSupplyGrams * fairPrice) * 100).toFixed(2);
    const viewProvision = +provision > 200 ? ' > 200' : provision;
    return (
      <div className={css.container}>
        {account.lastTxs && account.lastTxs.length > 0 && address && <div className={css.transactions}>
          <h2 className={css.title}>
            Last Transactions
          </h2>
          <table className={css.table}>
            <colgroup>
              <col className={css.colTimestamp} />
              <col className={css.colStatus} />
              <col className={css.colHash} />
              <col className={css.colTo} />
              <col className={css.colValue} />
            </colgroup>
            <thead className={css.thead}>
              <tr>
                <td className={css.theadTd}>TimeStamp</td>
                <td className={css.theadTd}>Status</td>
                <td className={css.theadTd}>TxHash</td>
                <td className={css.theadTd}>To</td>
                <td className={css.theadTd}>Value</td>
              </tr>
            </thead>
            <tbody className={css.tbody}>
              {account.lastTxs.map(tx => (
                <tr key={tx.hash}>
                  <td className={css.td}>{timeago().format(tx.timestamp)}</td>
                  {this.getTxStatus(tx.status)}
                  <td className={cn(css.td, css.tdHash)}>
                    <a className={css.link} target="_blank" href={`https://explorer.nebulas.io/#/tx/${tx.hash}`}>{tx.hash}</a>
                    </td>
                  <td className={css.td}>
                    <a className={css.link} target="_blank" href={`https://explorer.nebulas.io/#/address/${tx.to.hash}`}>{tx.to.hash}</a>
                  </td>
                  <td className={css.td}>{`${nasToDecimals(tx.value)} NAS`}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>}
        {!(account.lastTxs && account.lastTxs.length > 0) && address && <div className={css.transactions}><Spinner /></div>}
        <div className={css.sm}>
          <h2 className={css.title}>
            {!address && <button className={css.back} onClick={() => history.goBack()}>←</button>}
            Smart contract
          </h2>
          <div className={css.smItem}>
            <div className={css.key}>Provision:</div>
            <div className={cn(css.value, {
              [css.value_positive]: +provision >= 80,
              [css.value_warning]: +provision >= 40 && +provision < 80,
              [css.value_negative]: +provision < 40,
            })}>
              {`${viewProvision} %`}
            </div>
          </div>
          <div className={css.smItem}>
            <div className={css.key}>Balance:</div>
            <div className={css.value}>{`${contractBalanceNAS} NAS`}</div>
            
          </div>
          <div className={css.smItem}>
            <div className={css.key}>Total supply:</div>
            <div className={css.value}>{goldSupplyGrams}</div>
          </div>
          <div className={css.smItem}>
            <div className={css.key}>Smart contract:</div>
            <a
              className={`${css.value} ${css.link}`}
              target="_blank"
              href={`https://explorer.nebulas.io/#/tx/${hash}`}
            >
              {constants.CONTRACT_ADDRESS}
            </a>
          </div>
        </div>
      </div>
    )
  }

  getTxStatus = (status) => {
    switch(status) {
      case 0: 
        return <td className={cn(css.td, css.value_negative)}>Fail</td>
      case 1:
        return <td className={cn(css.td, css.value_positive)}>Success</td>
      case 2:
      default: 
        return <td className={cn(css.td, css.value_warning)}>Pending</td>
    }
  }
}

export default withRouter(Info);
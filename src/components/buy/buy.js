import React, { Component } from 'react';
import css from './buy.css';
import { PURE } from 'dx-util/lib/react/pure';
import { PriceContainer as Price } from '../../containers/price/price.container';
import constants from '../../constants';
import TxStatus from '../tx-status/tx-status';
import Button from '../../ui-kit/button/button';
import Input from '../../ui-kit/input/input';
import CustomSlider from '../../ui-kit/slider/slider';
import BigNumber from 'bignumber.js';
import axios from 'axios';
import NebPay from 'nebpay.js';
import { checkExtension } from '../../utils/extension.utils';

const nebPay = new NebPay();

@PURE
class Buy extends Component {
  state = {
    gold: '',
    nas: '',
    goldError: '',
    nasError: '',
    txHash: '',
    isInvalid: false,
    isExtension: true,
    error: '',
    isTxProcces: false,
    sliderValue: 0,
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.nas && this.props.buyPrice !== nextProps.buyPrice ) {
      const gold = (new BigNumber(this.state.nas).dividedBy(nextProps.buyPrice)).toFixed(constants.GOLD_DECIMALS);
      this.setState({
        gold
      })
    }
  }
 
  render() {
    const { gold, nas, goldError, nasError, txHash, isInvalid, error, isExtension, isTxProcces, sliderValue } = this.state;
    const { buyPrice } = this.props;

    return (
      <div className={css.container}>
        <h2 className={css.title}>Buy</h2>
        <div className={css.main}>
          <div> 
            <div className={css.form}>
              <span className={css.price}>
                <span className={css.priceText}>Current price:</span>
                <Price>{buyPrice}</Price>
              </span>
              <Input
                label="Payment NAS amount:"
                value={nas}
                disabled={!buyPrice || buyPrice == 0} 
                onChange={this.onChangeNas}
                error={nasError}
                onBlur={this.onNasBlur}
              />
              <CustomSlider value={sliderValue} onChange={this.onSliderChange} />
              <Input
                label="Receive GOLD amount:"
                value={gold}
                disabled={!buyPrice || buyPrice == 0} 
                onChange={this.onChangeGold}
                onBlur={this.onGoldBlur}
                error={goldError}
              />
              {!txHash && <Button onClick={this.onButtonClick} disabled={isInvalid}>Buy</Button>}
            </div>
            {error && <div className={css.error}>{error}</div>}
            {txHash && <TxStatus handleClear={this.handleBuyAgain} hash={txHash} action="BUY" />}
          </div>
          <div className={css.aside}>
            {!isExtension && <div className={css.extension}>
              Scan QRCode below using 
              <a
                className={css.extensionLink} 
                target="_blank"
                rel="noopener noreferrer"
                href="https://nano.nebulas.io/index_en.html"
              >
                NAS nano
              </a>
              or install
              <a 
                className={css.extensionLink} 
                target="_blank"
                rel="noopener noreferrer"
                href="https://github.com/nebulasio/WebExtensionWallet"
              >
                Web Extension Wallet
              </a>
              first. Thanks!
            </div>}
            {isExtension && isTxProcces && <div className={css.extension}>
              You can scan QRCode below using 
              <a
                className={css.extensionLink} 
                target="_blank"
                rel="noopener noreferrer"
                href="https://nano.nebulas.io/index_en.html"
              >
                NAS nano
              </a>
            </div>}
            {!txHash && <canvas className={css.qr} id="qr"/> }
          </div>
        </div>
      </div>
    )
  }

  onSliderChange = (rawValue) => {
    const { balance, buyPrice } = this.props;
    const balanceNas = new BigNumber(balance.nas);
    const value = balanceNas.multipliedBy(rawValue).dividedBy(100);
    const isEnough = balanceNas.minus(constants.DUST).minus(value).toFixed(constants.NAS_DECIMALS) > 0;

    if (rawValue === 100) {
      const newValue = value.minus(constants.DUST);
      if (newValue >= 0) {
        this.setState({
          nas: newValue.toFixed(constants.NAS_DECIMALS),
          gold: newValue.dividedBy(buyPrice).toFixed(constants.GOLD_DECIMALS),
        })
      }
    } else {
      this.setState({
        nas: value.toFixed(constants.NAS_DECIMALS),
        gold: value.dividedBy(buyPrice).toFixed(constants.GOLD_DECIMALS),
      })
    }

    if (isEnough || (rawValue === 100 && value.minus(constants.DUST) > 0)) {
      this.setState({
        goldError: '',
        nasError: '',
        isInvalid: false,
        error: '',
      })
    } else {
      this.setState({
        error: 'Inssufient balance',
        nasError: 'Inssufient balance',
        isInvalid: true,
      })
    }

    this.setState({
      sliderValue: rawValue,
    })
  }

  onNasBlur = () => {
    const oldNas = this.state.nas;
    if (oldNas && oldNas.length - 1 > constants.NAS_DECIMALS) {
      this.setState({
        nas: new BigNumber(oldNas).toFixed(constants.NAS_DECIMALS)
      })
    }
  }

  onGoldBlur = () => {
    const oldGold = this.state.gold;
    if (oldGold && oldGold.length - 1 > constants.GOLD_DECIMALS) {
      this.setState({
        gold: new BigNumber(oldGold).toFixed(constants.GOLD_DECIMALS)
      })
    }
  }

  handleBuyAgain = () => {
    this.setState({
      txHash: '',
    })
  }

  onChangeNas = (e) => {
    const { balance, buyPrice } = this.props;
    const value = e.target.value.replace(',', '.');
    const regex = /^[0-9]*[.]?[0-9]+$/;
    const balanceNas = new BigNumber(balance.nas);

    if (regex.test(value) && +value !== 0) {
      if (balanceNas.minus(constants.DUST).minus(value).toFixed(constants.NAS_DECIMALS) > 0) {
        
        this.setState({
          nasError: '',
          goldError: '',
          gold: (new BigNumber(value).dividedBy(buyPrice)).toFixed(constants.GOLD_DECIMALS),
          isInvalid: false,
          error: '',
          sliderValue: new BigNumber(value).multipliedBy(100).dividedBy(balanceNas),
        })
      } else {
        this.setState({
          isInvalid: true,
          error: 'Insufficient balance',
          nasError: 'Insufficient balance',
        })
      }
    } else {
      this.setState({
        nasError: 'Invalid value',
        isInvalid: true,
        error: '',
      })
    }

    this.setState({
      nas: value,
    })
  }

  onChangeGold = (e) => {
    const value = e.target.value.replace(',', '.');
    const regex = /^[0-9]*[.]?[0-9]+$/;
    const { buyPrice, balance } = this.props;
    const newNas = new BigNumber(value).multipliedBy(buyPrice).toFixed(constants.NAS_DECIMALS);
    if (regex.test(value) && +value !== 0 ) {
      if (+newNas <= +(new BigNumber(balance.nas).minus(constants.DUST).toFixed(constants.NAS_DECIMALS))) {
        this.setState({
          goldError: '',
          nasError: '',
          nas: newNas,
          isInvalid: false,
          error: '',
        })
      } else {
        this.setState({
          inInvalid: true,
          error: 'Insufficient balance',
          goldError: 'Insufficient balance',
        })
      }
    } else {
      this.setState({
        goldError: 'Invalid value',
        isInvalid: true,
        error: '',
      })
    }

    this.setState({
      gold: value,
    })
  }

  onButtonClick = () => {
    const { nas, gold } = this.state;
    if (!nas || !gold || +gold === 0 || +nas === 0) {
      this.setState({
        goldError: 'Empty field',
        nasError: 'Empty field',
      })
    } else {
      this.handleBuy();
    }
  }

  handleBuy = (account) => {
    const { buyPrice } = this.props;

    const options = {
      goods: {
        name: 'GOLD',
        desc: `You buying${this.state.gold} grams of gold at a price ${buyPrice}`
      },
      qrcode: {
        showQRCode: true,
        container: document.getElementById('qr')
      },
      listener: this.cbResponse,
    }

    this.setState({
      isTxProcces: true,
    })

    nebPay.call(
      constants.CONTRACT_ADDRESS,
      this.state.nas,
      'purchase',
      JSON.stringify([`${buyPrice}`]),
      options
    );

    this.setState({
      isExtension: checkExtension()
    })
  }

  cbResponse = (response) => {
    this.getTransactions();
    this.setState({
      txHash: response.txhash,
      isTxProcces: false,
    })
  }

  getTransactions = () => {
    const { address, actions } = this.props;
    if (address) {
      axios.get(`https://explorer.nebulas.io/main/api/address/${address}`).then(response => {
        const lastTxs = response.data.data.txList.slice(0, 5);
        actions.setLastTxs(lastTxs);
      });
    }
  }
}

export default Buy;
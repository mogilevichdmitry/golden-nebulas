import React, { Component } from 'react';
import { PURE } from 'dx-util/lib/react/pure';
import css from './trend.css';
import cn from 'classnames';

const STATUS = {
  UP: 'up',
  DOWN: 'down',
};

const VISIBILITY_DELAY = 3000;

@PURE
class Trend extends Component {
  state = {
    status: null,
    value: null,
    isVisible: false,
  }

  componentDidMount() {
    this.setState({
      value: this.props.value
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value > this.props.value) {
      this.setState({
        status: STATUS.UP,
        isVisible: true,
      })

      this.timeout = setTimeout(() => {
        this.setState({
          isVisible: false,
        })
        this.timeout = 0;
      }, VISIBILITY_DELAY);

    } else if (nextProps.value < this.props.value) {
      this.setState({
        status: STATUS.DOWN,
        isVisible: true,
      })

      this.timeout = setTimeout(() => {
        this.setState({
          isVisible: false,
        })
        this.timeout = 0;
      }, VISIBILITY_DELAY);
    } else {
      this.setState({
        status: null,
        isVisible: false,
      })
    }

    this.setState({
      value: nextProps.value
    })
  }
  componentWillUnmount() {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = 0;
    }
  }

  render () {
    const { status, isVisible } = this.state;

    return (
      <span className={cn(css.container, {
        [css.container_isVisible]: isVisible 
      })}>
        {status === STATUS.UP && <span className={css.up}>↑</span>}
        {status === STATUS.DOWN && <span className={css.down}>↓</span>}
      </span>
    )
  }
}

export default Trend;


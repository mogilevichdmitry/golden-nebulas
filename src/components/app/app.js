import React, { Component, Fragment } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { IntroductionContainer as Introduction } from '../../containers/introduction/introduction.container';
import { SidebarContainer as Sidebar } from '../../containers/sidebar/sidebar.container';
import { NebService } from '../../utils/neb.utils';
import { updatePrices } from '../../utils/smartcontract.utils';
import { updateSmartcontractExtraInfo } from '../../utils/smartcontract.utils';
import { ServiceContainer as Service } from '../../containers/service/service.container';
import { InfoContainer as Info } from '../../containers/info/info.container';
import css from './app.css';
import constants from '../../constants';
import NebPay from 'nebpay.js';
import axios from 'axios';
const nebPay = new NebPay();

class App extends Component {
  componentDidMount() {
    const { actions } = this.props;
    const { setBlockchainInfo, setSmartcontractInfo, setPrices } = actions;

    NebService.api.getNebState().then(state => {
      setBlockchainInfo(state);
    }).catch(err => console.log(err));
    this.getTokenData();
    updatePrices(setPrices);
    updateSmartcontractExtraInfo(setSmartcontractInfo);
    this.getTransactions();

    this.intervalUpdateSM = setInterval(() => updateSmartcontractExtraInfo(setSmartcontractInfo), 5000);
    this.intervalUpdateLastTxs = setInterval(() => this.getTransactions(), 30000);

    nebPay.simulateCall(constants.ECHO_ADDRESS, 0, 'echo', '', {
      listener: (response) => {
        this.props.actions.setAddress(JSON.parse(response.result));
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.address !== nextProps.address) {
      this.getTransactions(nextProps.address);
    }
  }

  componentWillUnmount() {
    clearInterval(this.intervalUpdateSM);
    clearInterval(this.intervalUpdateLastTxs);
  }

  render() {
    return (
      <div className={css.container}>
        <Sidebar />
        <div className={css.content}>
          <Switch>
            <Route exact path="/" component={Introduction} />
            <Route path="/info" component={Info} />
            <Route component={props => <Protected {...props} isAuthenticated={!!this.props.address} />} />
          </Switch>
        </div>
      </div>
    )
  }

  getTokenData = () => {
    NebService.api.call({
      from: constants.CONTRACT_ADDRESS,
      to: constants.CONTRACT_ADDRESS,
      contract: {
        function: 'tokenData',
      },
      value: '0',
      gasPrice: constants.GAS_PRICE,
      gasLimit: constants.GAS_LIMIT,
      nonce: 0,
    }).then(data => { 
      this.props.actions.setSmartcontractInfo(JSON.parse(data.result));
    }).catch(err => {
      console.log(err);
    });
  }

  getTransactions = (newAddress) => {
    const { address, actions } = this.props;
    if (newAddress || address) {
      axios.get(`https://explorer.nebulas.io/main/api/address/${newAddress || address}`).then(response => {
        const lastTxs = response.data.data.txList.slice(0, 5);
        actions.setLastTxs(lastTxs);
      });
    }
  }
}

const Protected = ({ isAuthenticated }) => {
  if (!isAuthenticated) {
    return <Redirect to="/" />
  }

  return (
    <Fragment>
      <Route path="/service" component={props => <Service {...props} />} />
    </Fragment>
  );
}

export default App;
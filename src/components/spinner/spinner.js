import React, { Component } from 'react';
import css from './spinner.css';
import { PURE } from 'dx-util/lib/react/pure';

@PURE
class Spinner extends Component {
  render() {
    return (
      <div className={css.loader} />
    )
  }
}

export default Spinner;

export const formatBalance = (number, n = 3) => {
  const N = Math.pow(10, n);
  const formated = (Math.floor(number * N)) / N;
  let value = formated.toFixed(n);

  if (value == 0) { // eslint-disable-line
    return 0;
  } else {
    for (let i = value.length; i > 0; i--) {
      if (value[i - 1] === '0') {
        value = value.slice(0, -1);
      } else {
        break;
      }
    }
    if (value.indexOf('.') === value.length - 1) {
      return value.substring(0, value.length - 1);
    } else {
      return value;
    }
  }
}
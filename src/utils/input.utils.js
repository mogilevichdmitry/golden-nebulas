export const isValidOnKeyPress = (e, decimals) => {
  const v = e.target.value;
  if (v.indexOf('.') > -1 && v.split('.')[1].length === decimals && e.keyCode !== 46 && e.keyCode !== 8) {
    return true;
  }
  return false;
}
import { Neb, HttpRequest } from 'nebulas';

export const NebService = new Neb();
NebService.setRequest(new HttpRequest('https://mainnet.nebulas.io'));
import { NebService } from './neb.utils';
import constants from '../constants';
import { goldToDecimals } from '../utils/gold.uitls';

export const getDust = (address, value, price) => {
  console.log(Number(value), 'number');
  return (
    NebService.api.call({
      from: address,
      to: constants.CONTRACT_ADDRESS,
      contract: {
        function: 'purchase',
        args: JSON.stringify([`${price}`]),
      },
      value: Number(value),
      gasPrice: constants.GAS_PRICE,
      gasLimit: constants.GAS_LIMIT,
      nonce: 0,
    }).then(data => {
      console.log(JSON.parse(JSON.parse(data.result.estimate_gas)));
      return 1;
    }).catch(err => {
      console.log(err);
    })
  )
}

export const updateGoldBalance = (address, setGoldBalance) => (
  NebService.api.call({
    from: address,
    to: constants.CONTRACT_ADDRESS,
    contract: {
      function: 'balanceOf',
    },
    value: '0',
    gasPrice: constants.GAS_PRICE,
    gasLimit: constants.GAS_LIMIT,
    nonce: 0,
  }).then(data => {
    const value = goldToDecimals(JSON.parse(JSON.parse(data.result)));
    setGoldBalance(value);
  }).catch(err => {
    console.log(err);
  })
)

export const updatePrices = (setPrices) => (
  NebService.api.call({
    from: constants.CONTRACT_ADDRESS,
    to: constants.CONTRACT_ADDRESS,
    contract: {
      function: 'prices',
    },
    value: '0',
    gasPrice: constants.GAS_PRICE,
    gasLimit: constants.GAS_LIMIT,
    nonce: 0,
  }).then(data => {
    const result = JSON.parse(data.result);
    setPrices(result);
  }).catch(err => {
    console.log(err);
  })
)

export const updateSmartcontractExtraInfo = (setSmartcontractInfo) => (
  NebService.api.call({
    from: constants.CONTRACT_ADDRESS,
    to: constants.CONTRACT_ADDRESS,
    contract: {
      function: 'moreData',
    },
    value: '0',
    gasPrice: constants.GAS_PRICE,
    gasLimit: constants.GAS_LIMIT,
    nonce: 0,
  }).then(data => {
    const result = JSON.parse(data.result);
    setSmartcontractInfo(result);
  }).catch(err => {
    console.log(err);
  })
)

import { BigNumber } from 'bignumber.js';
import CONSTANTS from '../constants';

export const nasToDecimals = (value) => {
  const n = new BigNumber(value);
  return n.div(new BigNumber(10).pow(CONSTANTS.NAS_DECIMALS));
};

export const nasFromDecimals = (value) => {
  const n = new BigNumber(value);

  return n.multipliedBy(new BigNumber(10).pow(CONSTANTS.NAS_DECIMALS));
};

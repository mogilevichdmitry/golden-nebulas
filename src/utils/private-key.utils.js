export const isPrivateKeyValid = (privateKey) => {
  const regex = /[0-9A-Fa-f]{6}/g;

  if (regex.test(privateKey) && privateKey.length === 64) {
    return true;
  }

  return false;
}


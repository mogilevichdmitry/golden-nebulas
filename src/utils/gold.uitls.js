import { BigNumber } from 'bignumber.js';
import CONSTANTS from '../constants';

export const goldToDecimals = (value) => { // /
  const n = new BigNumber(value);
  return n.div(new BigNumber(10).pow(CONSTANTS.GOLD_DECIMALS));
};

export const goldFromDecimals = (value) => { // *
  const n = new BigNumber(value);
  return n.multipliedBy(new BigNumber(10).pow(CONSTANTS.GOLD_DECIMALS));
};

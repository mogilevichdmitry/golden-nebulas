import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { AppContainer as App } from './containers/app/app.container';
import { HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { loadState, saveState } from './utils/local-storage.utils';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers';
import { unregister } from './registerServiceWorker';

const composeEnhancers = typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

const enhancer = composeEnhancers(applyMiddleware(thunk));
const persistedState = loadState();
const store = createStore(rootReducer, persistedState, enhancer);

store.subscribe(() => {
  saveState({
    address: store.getState().address,
    smartcontract: store.getState().smartcontract,
  });
});

ReactDOM.render(
  <Provider store={store}>
    <HashRouter>
      <App />
    </HashRouter>
  </Provider>, document.getElementById('root')
);

unregister();
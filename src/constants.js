import BigNumber from 'bignumber.js';

const GAS_PRICE = 1000000;
const GAS_LIMIT = 24000;
const NAS_DECIMALS = 18;
const GOLD_DECIMALS = 9;
const CONTRACT_ADDRESS = 'n1nhxd6C3qpVoWLBUqySdURoQfhPMB1rXBw';
const ECHO_ADDRESS = 'n1oDt1qXFiTT1y67CMr6syWZcKpCGDCHu8P';
// const DUST = (GAS_LIMIT * GAS_PRICE / Math.pow(10, 18)) * 2;
const DUST = new BigNumber(GAS_LIMIT).multipliedBy(GAS_PRICE).dividedBy(Math.pow(10, 18)).multipliedBy(10);

export default {
  GAS_PRICE,
  GAS_LIMIT,
  NAS_DECIMALS,
  GOLD_DECIMALS,
  CONTRACT_ADDRESS,
  ECHO_ADDRESS,
  DUST,
}